const Koa = require('koa');
const Router = require('koa-router');
const fs = require('fs');
const koaBody = require('koa-body');
const amqp = require('amqplib/callback_api');

const app = new Koa();
const router = new Router();
const exChangeName = 'email_post';
const util = require('util');
let amqpConnect = util.promisify(amqp.connect);
let createChannel;
const PORT = process.env.PORT || 3000;

app.use(koaBody());
let cacheTemplate = fs.readFileSync(`${__dirname}/template.html`).toString();

// connect rabbit mq
(async () => {
    amqpConnect = await amqpConnect(process.env.CONNECT);
    createChannel = util.promisify(amqpConnect.createChannel)
})();

router.get('/', (ctx, next) => {
    ctx.response.header = {
    'Content-Type' :'text/html; charset=utf-8'
        };
    ctx.body = cacheTemplate;
});
router.post('/', (ctx, next) => {
    (async () => {
        let ch = await createChannel.call(amqpConnect);
            let key = 'post';
            // // точка доступа имя, тип
            ch.assertExchange(exChangeName, 'topic', {durable: false});
            // // устанавливем routing-key
            ch.publish(exChangeName, key, Buffer.from(JSON.stringify(ctx.request.body)));
            console.log(" [x] Sent %s:'%s'", key, JSON.stringify(ctx.request.body));
    })();
    return ctx.body = cacheTemplate;
});

app
    .use(router.routes())
    .use(router.allowedMethods());
app.listen(PORT);
console.log(`http://localhost:${PORT}`);
