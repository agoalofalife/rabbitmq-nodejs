#!/usr/bin/env node
require('dotenv').config();
const amqp = require('amqplib/callback_api');
const util = require('util');
const exChangeName = 'email_post';
let amqpConnect = util.promisify(amqp.connect);
const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
    "host": "smtp.gmail.com",
    "port": 465,
    "secure": true,
    "auth": {
        "user": process.env.EMAIL_USER,
        "pass": process.env.EMAIL_PASSWORD
    }
});

(async () => {
    amqpConnect = await amqpConnect(process.env.CONNECT);
    let createChannel = util.promisify(amqpConnect.createChannel);
    let ch = await createChannel.call(amqpConnect);

    ch.assertExchange(exChangeName, 'topic', {durable: false});

    let q = await util.promisify(ch.assertQueue).call(ch, '', {exclusive: true});
    console.log(' [*] Waiting for logs. To exit press CTRL+C');
    // binding очередь и exchange и ключ
    ch.bindQueue(q.queue, exChangeName, 'post');

    ch.consume(q.queue, function(msg) {
        console.log( msg.content.toString() );
        let options = JSON.parse(msg.content.toString());

        let mailOptions = {
            from: `"${process.env.EMAIL_USER}" <${process.env.EMAIL_USER}>`,
            to: options.address,
            subject: options.subject,
            text: options.text.trim().slice(0, 500) +
            `\n Отправлено с: <${process.env.EMAIL_USER}>`
        };

        transporter.sendMail(mailOptions).then(res => {
            console.log( res );
        }).catch(err => {
            console.log( err );
        });

        console.log(" [x] %s:'%s'", msg.fields.routingKey, msg.content.toString());
    }, {noAck: true});
})();
