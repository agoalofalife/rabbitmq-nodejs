#!/usr/bin/env node

const amqp = require('amqplib/callback_api');
const exChangeName = 'topic_logs';

amqp.connect(process.env.CONNECT, function(err, conn) {
    conn.createChannel(function(err, ch) {

        let args = process.argv.slice(2);
        // устанавливаем ключ для нашей темы
        let key = (args.length > 0) ? args[0] : 'anonymous.info';
        let msg = args.slice(1).join(' ') || 'Hello World!';

        // точка доступа имя, тип
        ch.assertExchange(exChangeName, 'topic', {durable: false});
        // устанавливем routing-key
        ch.publish(exChangeName, key, Buffer.from(msg));
        console.log(" [x] Sent %s:'%s'", key, msg);
    });

    setTimeout(function() { conn.close(); process.exit(0) }, 500);
});