#!/usr/bin/env node

const amqp = require('amqplib/callback_api');
const args = process.argv.slice(2);
const exChangeName = 'topic_logs';

if (args.length === 0) {
    console.log("Usage: receive_logs_topic.js <facility>.<severity>");
    process.exit(1);
}

amqp.connect(process.env.CONNECT, function(err, conn) {
    conn.createChannel(function(err, ch) {
        ch.assertExchange(exChangeName, 'topic', {durable: false});

        // создаем (анонимную) очередь
        ch.assertQueue('', {exclusive: true}, function(err, q) {
            console.log(' [*] Waiting for logs. To exit press CTRL+C');

            // binding очередь и exchange и ключ
            args.forEach(function(key) {
                ch.bindQueue(q.queue, exChangeName, key);
            });

            // обработка
            ch.consume(q.queue, function(msg) {
                console.log(" [x] %s:'%s'", msg.fields.routingKey, msg.content.toString());
            }, {noAck: true});
        });
    });
});