#!/usr/bin/env node
const queueName = 'hello';
const amqp = require('amqplib/callback_api');

// подключение
amqp.connect(process.env.CONNECT, function(err, conn) {
    conn.createChannel(function(err, ch) {
        ch.assertQueue(queueName, {durable: false});

        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queueName);

        // ждем асинхронных сообщений
        ch.consume(queueName, function(msg) {
            console.log(" [x] Received %s", msg.content.toString());
        }, {noAck: true}); // noAck рассмотрим позже
    });
});