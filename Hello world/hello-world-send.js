#!/usr/bin/env node

// подключаем модуль для работы с протоколом AMQP
const amqp = require('amqplib/callback_api');
const queueName = 'hello';

// соединение
amqp.connect(process.env.CONNECT, function(err, conn) {
    // создание канала
    conn.createChannel(function(err, ch) {
        // проверяем что очередь есть,если ее нет , то создаем
        // метод является идемпотентным(idempotent)
        ch.assertQueue(queueName, {durable: false});

        ch.sendToQueue(queueName, Buffer.from('Hello World!'));
        console.log(" [x] Отправили 'Hello World!'");
    });
    // timeout на закрытие соединения
    setTimeout(function() { conn.close(); process.exit(0) }, 500);
});

// после выполним команду, посмотреть список очередей
// sudo rabbitmqctl list_queues


// sudo rabbitmqctl list_queues name messages_unacknowledged messages_ready messages durable auto_delete consumers | grep -v "\.\.\." | sort | column -t;

// скрипт выводит и обновляет каждые 2 секунды таблицу со списком очередей:
// имя очереди;
// количество сообщений в обработке;
// количество сообщений готовых к обработке;
// общее количество сообщений;
// устойчивость очереди к перезагрузке сервиса;
// является ли временной очередью;
// количество подписчиков;
