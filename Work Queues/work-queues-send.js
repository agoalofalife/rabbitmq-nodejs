#!/usr/bin/env node

// подключаем модуль для работы с протоколом AMQP
const amqp = require('amqplib/callback_api');
const queue = 'task_queue';

// соединение
amqp.connect(process.env.CONNECT, function(err, conn) {
    // создание очереди
    conn.createChannel(function(err, ch) {

        // сообщение берется из аргументов командной строки
        // точка будет равно одной секунде, тем самым мы создаем(имитирум ) нагрузку(payload)
        // Например Hello... будет выполнятеся 3 секунды.
        let msg = process.argv.slice(2).join(' ') || "Hello World!";

        ch.assertQueue(queue, {durable: true});
        // durable значит что при создание очереди
        // мы ее объявляем как устойчивую
        // устойчивую к падению и остановки RabbitMQ

        // На предыдущем шаге мы создали очерель 'hello'
        // Rabbit не дает переопределить существующую очередь новыми параметрами
        // мы просто создаем новую
        ch.sendToQueue(queue, Buffer.from(msg), {persistent: true});
        // persistent
        // помечаем сообщение как устойчивое
        //
        // есть нюанс что это не дает нам гарантии что сообщение не будет утеряно
        // есть промежуток времени когда RabbitMQ подтвердил но еще не успел записат на диск
        // и насколько я понял он использует mmap, и не делает fsync для каждого сообщения
        // поэтому есть промежуток времени когда записано в кэш, но не сохраненно на диск.

        // для более устойчивости мы можем использывать Publisher Confirms https://www.rabbitmq.com/confirms.html
        console.log(" [x] Sent '%s'", msg);
    });
    // разрываем соединение
    setTimeout(function() { conn.close(); process.exit(0) }, 500);
});
