#!/usr/bin/env node
const queue = 'task_queue';
const amqp = require('amqplib/callback_api');

// подключение
amqp.connect(process.env.CONNECT, function(err, conn) {
    conn.createChannel(function(err, ch) {
        ch.assertQueue(queue, {durable: true});

        // не отдавать подписчику единовременно более одного сообщения
        ch.prefetch(1);

        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);

        ch.consume(queue, function(msg) {
            // определем кол-во сек по точкам
            let secs = msg.content.toString().split('.').length - 1;

            console.log(" [x] Received %s", msg.content.toString());
            // эмулируем ресурсоемкую задачу, кол - во точек в предлодежении
            // равняется кол - ву  секунд
            setTimeout(function() {
                console.log(" [x] Done");
                // отправка уведдомления что закончена обработка
                // если мы забудем отправить ask, сообщения будут повторно добавлены
                // rabbit начнет потреблять память, тем самым мы создадим утечку
                // чтобы отладить эту ошибку мы можем использывать команду
                // sudo rabbitmqctl list_queues name messages_ready messages_unacknowledged
                ch.ack(msg);
            }, secs * 1000);

        }, {noAck: false});
        // для чего параметр noAck
        // если во время работы с прервется работа(напрмер просто прервем в консоли)
        // то сообщение из очереди потеряется и не будет обработано
        // Rabbit после отправки consumer помечает сообщение как полученное и удаляет его

        // мы должны иметь возможность выполнять так называемую транзакцию , иметь
        // возможность откатывать операцию если она не была обработана или наоборот

        // в false режим включен, в конце работы мы должны отправить обратно
        // уведомление ch.ack(msg);

        // теперь без подтверждение от consumer Rabbit не будет удалять сообщение
    });
});