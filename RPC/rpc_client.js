#!/usr/bin/env node

const amqp = require('amqplib/callback_api');
const args = process.argv.slice(2);

if (args.length == 0) {
    console.log("Usage: rpc_client.js num");
    process.exit(1);
}

amqp.connect(process.env.CONNECT, function(err, conn) {

    conn.createChannel(function(err, ch) {
        ch.assertQueue('', {exclusive: true}, function(err, q) {
            // генерируем номер correlation
            let corr = generateUuid();
            // передаем число для обработки в очередь
            let num = parseInt(args[0]);

            console.log(' [x] Requesting fib(%d)', num);

            // обработка ответа от rpc server
            ch.consume(q.queue, function(msg) {
                if (msg.properties.correlationId === corr) {
                    console.log(' [.] Got %s', msg.content.toString());
                    setTimeout(function() { conn.close(); process.exit(0) }, 500);
                }
            }, {noAck: true});

            // отправка в очередь число
            ch.sendToQueue('rpc_queue',
                Buffer.from(num.toString()),
                { correlationId: corr, replyTo: q.queue });
        });
    });
});

// generate Correlation id
function generateUuid() {
    return Math.random().toString() +
        Math.random().toString() +
        Math.random().toString();
}