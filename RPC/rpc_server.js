#!/usr/bin/env node

const amqp = require('amqplib/callback_api');
const rpc_queue = 'rpc_queue';

amqp.connect(process.env.CONNECT, function(err, conn) {
    conn.createChannel(function(err, ch) {

        // создаем стабильную очередь
        ch.assertQueue(rpc_queue, {durable: false});
        // максимально кол-во в обработке не больше одного
        // если мы захотим распределить нагрузку между несколькими серверами
        ch.prefetch(1);

        console.log(' [x] Awaiting RPC requests');

        ch.consume(rpc_queue, function reply(msg) {
            // принимаем число
            let n = parseInt(msg.content.toString());

            // получаем число от клиента
            console.log(" [.] fib(%d)", n);

            // обрабаытываем его
            let r = fibonacci(n);

            // отправляем обратно клиенту , через временную очередь из replyTo
            ch.sendToQueue(msg.properties.replyTo,
                Buffer.from(r.toString()),
                {correlationId: msg.properties.correlationId});
            // ответ что обработка закончена
            ch.ack(msg);
        });
    });
});

function fibonacci(n) {
    if (n === 0 || n === 1)
        return n;
    else
        return fibonacci(n - 1) + fibonacci(n - 2);
}