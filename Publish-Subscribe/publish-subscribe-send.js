#!/usr/bin/env node
const amqp = require('amqplib/callback_api');
const exChangeName = 'logs';

// подключение
amqp.connect(process.env.CONNECT, function(err, conn) {

    conn.createChannel(function(err, ch) {
        // берем сообщение из консоли
        let msg = process.argv.slice(2).join(' ') || 'Hello World!';

        // Устанавливаем exchange
        ch.assertExchange(exChangeName, 'fanout', {durable: false});
        // публикуем сообщение в точку обмена

        // пустая строка говорит что мы не хотим опубликовать в конкретную очередь
        // мы хотим просто отправить в точку обмена
        ch.publish(exChangeName, '', Buffer.from(msg));
        console.log(" [x] Sent %s", msg);
    });
    // close connection
    setTimeout(function() { conn.close(); process.exit(0) }, 500);
});