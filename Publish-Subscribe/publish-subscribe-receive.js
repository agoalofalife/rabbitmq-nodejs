#!/usr/bin/env node

const amqp = require('amqplib/callback_api');
const exChangeName = 'logs';

amqp.connect(process.env.CONNECT, function(err, conn) {
    conn.createChannel(function(err, ch) {
        // так же устанавливаем точку обмена
        ch.assertExchange(exChangeName, 'fanout', {durable: false});

        // — Каждый раз когда мы соединяемся с Rabbit, мы создаем новую очередь, или даем создать серверу случайное наименование;
        //  — Каждый раз когда подписчик отключается от Rabbit, мы удаляем очередь.

        // В  клиенте, когда мы обращаемся к очереди без наименовании, мы создаем временную очередь
        // и автоматически сгенерированным наименованием:
        ch.assertQueue('', {exclusive: true}, function(err, q) {

            console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
            // мы говорим что соединяем точку доступа(exchange) с нашей случаной очередью.
            ch.bindQueue(q.queue, exChangeName, '');

            ch.consume(q.queue, function(msg) {
                console.log(" Get [x] %s", msg.content.toString());
            }, {noAck: true});
        });
    });
});