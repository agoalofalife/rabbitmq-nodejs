#!/usr/bin/env node

const amqp = require('amqplib/callback_api');
const args = process.argv.slice(2);
const exChangeName = 'direct_logs';

if (args.length == 0) {
    console.log("Usage: receive_logs_direct.js [info] [warning] [error]");
    process.exit(1);
}

amqp.connect(process.env.CONNECT, function(err, conn) {
    conn.createChannel(function(err, ch) {

        // устанавливаем точку доступа
        ch.assertExchange(exChangeName, 'direct', {durable: false});

        // устанавливаем временную очередь
        ch.assertQueue('', {exclusive: true}, function(err, q) {
            console.log(' [*] Waiting for logs. To exit press CTRL+C');

            // связываем для всех указанных "маршрутов"(error, warning...)
            args.forEach(function(severity) {
                ch.bindQueue(q.queue, exChangeName, severity);
            });

            ch.consume(q.queue, function(msg) {
                console.log(" [x] %s: '%s'", msg.fields.routingKey, msg.content.toString());
            }, {noAck: true});
        });
    });
});