### Описание 

 В этой статье модернизируем нашу программу — будем отправлять получателю только часть сообщений. 
 Например, мы сможем сохранять на диске только сообщения с критическими ошибками (экономия места на диске), а в консоли будем отображать все сообщения.
 
 #### Bindings
  
```javascript
ch.bindQueue(q.queue, exChangeName, '');
```

Binding — это связь между точкой доступа и очередью.
Это можно интерпретировать как: очередь хочет получить сообщения из точки доступа. 

Binding может принимать параметр `binding_key`.

```javascript
ch.bindQueue(queue_name, exChangeName, 'black');
```

Значение этого ключа зависит от типа точки доступа.
Точка доступа с типом `fanout` просто проигнорирует его.

#### Точка доступа Direct

Наша система логирования в предыдущей статье отправляла всем подписчикам - все сообщения. 
Мы хотим расширить нашу систему, чтобы фильтровать сообщения по степени важности.
Для примера мы сделаем так, чтобы скрипт, записывающий логи на диск не тратил своё место на собщения с типом `warning` или `info`.
Ранее мы использовали точку доступа с типом `fanout`, которая не дает нам полной гибкости — она подходит только для простой трансляции. 
Вместо это мы будем использовать тип `direct`.
Его алгоритм очень прост — сообщения идут в ту очередь, `binding_key` которой совпадает с `routing key` сообщения.

![alt](https://habrastorage.org/getpro/habr/post_images/7e6/e66/dc6/7e6e66dc6dc2671db78878e0a28f1b9a.png)

На схеме хорошо видно, у нас есть `exchange` с типом `direct` и две связаные очереди.
Первая очередь связана с `binding key = orange`, а вторая очередь имеет две связи.
Одна с ключом `binding key = black`, а вторая с ключом — `green`.
Сообщения с routing key = orange будут направляться в очередь Q1, а сообщения с ключом black или green направятся в очередь Q2. Все остальные сообщения будут удалены. 

#### Составные связи (Multiple bindings)

![alt](https://habrastorage.org/getpro/habr/post_images/75d/79c/a88/75d79ca88de6629904dc8b1dfc8a9f56.png)

Вполне допустимо связывать несколько очередей с одинаковым ключом `binding key`. 
В этом примере мы связываем точку доступа X и очередь Q1 с тем же ключом `black`, что и у очереди `Q2`.
В этом примере `direct` ведет себя также как и `fanout`: отсылает сообщения во все связанные очереди.
Сообщения с ключом `black` попадет в обе очереди `Q1` и `Q2`.

Итого

![alt](https://habrastorage.org/getpro/habr/post_images/090/365/572/0903655728106b27e853b101d939af9b.png)

Для выполнения работы:
Подписчик для типа `warning` или `error`

```shell
node Routing/routing-receive.js warning error > logs_from_rabbit.log
```
Подписчик для типа `info` , `warning` или `error`
```shell
node Routing/routing-receive.js info
```

Отправляем в очередь сообщение об ошибки
 
```bash
node Routing/routing-send.js error "Run. Run. Or it will explode."
```
