#!/usr/bin/env node

const amqp = require('amqplib/callback_api');
const exChangeName = 'direct_logs';

amqp.connect(process.env.CONNECT, function(err, conn) {
    conn.createChannel(function(err, ch) {
        let args = process.argv.slice(2);
        let msg = args.slice(1).join(' ') || 'Hello World!';

        // первым аргументом routing_bind, тип сообщения
        let severity = (args.length > 0) ? args[0] : 'info';

        // Устанавливаем точку доступа(ex-change) с типом direct
        ch.assertExchange(exChangeName, 'direct', {durable: false});

        // при отправке указываем routing key
        ch.publish(exChangeName, severity, Buffer.from(msg));
        console.log(" [x] Sent %s: '%s'", severity, msg);
    });

    setTimeout(function() { conn.close(); process.exit(0) }, 500);
});